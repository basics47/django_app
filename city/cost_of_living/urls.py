from django.urls import path
from . import views
from .views import dashboard, CityListView, HistoryView, CityView, CityUpdate, CityDelete
from django.conf import settings
from django.conf.urls.static import static

app_name = 'cost_of_living'

urlpatterns = [
    path('', views.main, name='main'),
    path("search/", dashboard, name="dashboard"),
    path("cities", CityListView.as_view(), name="citylistview"),
    path('history_of_searching', HistoryView.as_view(), name='history'),
    path('city_detail/<pk>', CityView.as_view(), name="city_details"),
    path('city_update/<pk>', CityUpdate.as_view(), name="update"),
    path('city_delete/<pk>', CityDelete.as_view(), name="delete"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)