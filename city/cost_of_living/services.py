import os
import requests

def get_data_from_api(city, country):

    url = "https://cost-of-living-and-prices.p.rapidapi.com/prices"

    querystring = {"city_name":city,"country_name":country}

    headers = {
        "X-RapidAPI-Key": os.environ['API_KEY'],
        "X-RapidAPI-Host": "cost-of-living-and-prices.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.text)

    return response.json()