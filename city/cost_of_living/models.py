from django.db import models

class City(models.Model):
    name = models.CharField(max_length=50)
    population = models.IntegerField(null=True)
    country = models.CharField(max_length=50)
    updated = models.DateTimeField(null=True, blank=True)
    currency = models.CharField(max_length=20, null=True, blank=True)
    cost_of_apples = models.CharField(max_length=50, null=True, blank=True)
    cost_of_jeans = models.CharField(max_length=50, null=True, blank=True)
    cost_of_cinema_ticket = models.CharField(max_length=50, null=True, blank=True)

class HistoryOfSearching(models.Model):
    city = models.ForeignKey('cost_of_living.City', null=True, on_delete=models.CASCADE)
    searched = models.DateTimeField(auto_now_add=True)
