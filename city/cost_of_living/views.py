from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, UpdateView
from django.views.generic.list import ListView
from django.shortcuts import render, redirect

from django.http import HttpResponse
from .models import City, HistoryOfSearching
from .forms import CityForm
from .services import get_data_from_api

def main(request):
    message = '<h1>This is the films MAIN page.</h1>'
    return HttpResponse(message)

def dashboard(request):
    if request.method == 'POST':
        form = CityForm(request.POST)
        if form.is_valid():
            City.objects.get_or_create(name=form.cleaned_data['name'], country=form.cleaned_data['country'])
            city_obj = City.objects.get(name=form.cleaned_data['name'], country=form.cleaned_data['country'])
            if not city_obj.cost_of_apples:
                x=get_data_from_api(city=form.cleaned_data['name'], country=form.cleaned_data['country'])
                city_obj.cost_of_apples = x['prices'][7]['avg']
                city_obj.cost_of_cinema_ticket = x['prices'][37]['avg']
                city_obj.cost_of_jeans = x['prices'][4]['avg']
                city_obj.save()
            search = HistoryOfSearching(city=city_obj)
            search.save()
            return redirect("cost_of_living:history")
        else:
            context = {
            'form':form,
        }
    else:
        form = CityForm()
        context = {
            'form':form,
            'cities':City.objects.all(),
        }
    return render(request, "cities/city_form.html", context)

class CityListView(ListView):

    model = City
    template_name = 'cities/list_of_cities.html'
    paginate_by = 10
    ordering = ['name']
    

class HistoryView(ListView):

    model = HistoryOfSearching
    template_name = 'cities/history_of_searching.html'
    paginate_by = 10
    ordering = ['searched']


class CityView(DetailView):

    model = City
    template_name = 'cities/city_detail.html'


class CityUpdate(UpdateView):

    model = City
    template_name = 'cities/city_update_form.html'
    fields = ['country']
    success_url='/cost_of_living/'

class CityDelete(DeleteView):

    model = City
    template_name = 'cities/city_delete.html'
    success_url='/cost_of_living/'